(function()
{
    if (!window.MEDLINE_WEBPACK_FEDERATION_UTILS) {
        window.MEDLINE_WEBPACK_FEDERATION_UTILS = {};
      }

      window.MEDLINE_WEBPACK_FEDERATION_UTILS.waitForLoadComponent = function(fn)
      {
        var waitForLoadComponent = window.MEDLINE_WEBPACK_FEDERATION_UTILS.waitForLoadComponent;

        if(!window.MEDLINE_WEBPACK_FEDERATION_UTILS.loadComponent)
        {
          setTimeout(function() { waitForLoadComponent(fn); }, 500);
        }
        else
        {
          fn();
        }
    }
})();

