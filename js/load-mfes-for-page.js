window.MEDLINE_WEBPACK_FEDERATION_UTILS.waitForLoadComponent(function()
{
    /*
        Angular polyfills is required to load ANYTHING angular, INCLUDING micro frontends,
        so load at least one reference to polyfills that has zone js included
    */
    window.MEDLINE_WEBPACK_FEDERATION_UTILS.loadComponent("mfeHeader", "./MFEHeaderPolyfills")();

    // Load Angular micro frontends
    window.MEDLINE_WEBPACK_FEDERATION_UTILS.loadComponent("mfeHeader", "./MFEHeaderBootstrap")();

    window.MEDLINE_WEBPACK_FEDERATION_UTILS.loadComponent("eComCustomerPreference", "./PreferenceDialogComponentStyles")();
    window.MEDLINE_WEBPACK_FEDERATION_UTILS.loadComponent("eComCustomerPreference", "./PreferenceDialogComponentBootstrap")();


    var trigger = document.getElementById("trigger");
    var toggleOn = false;
    trigger.onclick = function()
    {
        toggleOn = !toggleOn;

        var modalInstance = document.getElementById("example-modal");

        if(toggleOn)
        {
            modalInstance.preferences = [
                { label: "Test Checkbox 1", id: "chk1" },
                { label: "Test Checkbox 2", id: "chk2", checked: true },
                { label: "Test Checkbox 3", id: "chk3", checked: true },
                { label: "Test Checkbox 3", id: "chk4" }
            ];
        }

        var customEvent = new CustomEvent("ecomCustomerPreferenceOpenDialog", {bubbles: true});
        modalInstance.dispatchEvent(customEvent);
    };


    var closeDialog = function(e) {
        const evnt =  new CustomEvent(
        'ecomCustomerPreferenceCloseDialog',
        { bubbles: true }
        );

        document.querySelector("ecom-customer-preference-dialog").dispatchEvent(evnt);
    };

    document.addEventListener('ecomCustomerPreferenceCloseDialog', closeDialog);
    document.addEventListener('ecomCustomerPreferenceDismissDialog', closeDialog);
});